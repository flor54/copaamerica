package interfaz;

import logic.ArmadoGrupos;
import logic.Grupo;

public class ObserverConsola implements ArmadoGrupos.Observer
{
	private ArmadoGrupos gruposArmados;
	
	public ObserverConsola(ArmadoGrupos grupos)
	{
		gruposArmados = grupos;
	}

	@Override
	public void update()
	{
		String ret = "It " + gruposArmados.getIteracion() + ":\n";
		for(Grupo g: gruposArmados.getGrupos())
			ret= ret + g.toString();
		ret = ret + "-Tiempo: " + gruposArmados.getTiempo();
		System.out.println("-----\n"+ret + "\n-----");
	}
}
