package main;

import java.util.ArrayList;

import interfaz.ObserverConsola;
import logic.ArmadoGrupos;
import logic.Equipo;

public class Main
{
	public static void main(String[] args)
	{
		// muestra las combinaciones que se van armando durante la ejecucion junto con la dispersion de cada combinacion de grupos
		// armando.
		
		ArrayList<Equipo> equipos = new ArrayList<Equipo>();
		
		equipos.add(new Equipo("Brasil",9));
		equipos.add(new Equipo("Venezuela",5));
		equipos.add(new Equipo("Colombia", 8));
		equipos.add(new Equipo("Peru",4));
		equipos.add(new Equipo("Qatar",3));
		equipos.add(new Equipo("Uruguay",6));
		equipos.add(new Equipo("Japon",4));
		equipos.add(new Equipo("Argentina", 2));
		equipos.add(new Equipo("Ecuador", 4));
		equipos.add(new Equipo("Paraguay",3));
		equipos.add(new Equipo("Chile",2));
		equipos.add(new Equipo("Bolivia", 3));
		
		ArmadoGrupos solver = new ArmadoGrupos(equipos);
		
		solver.registrar(new ObserverConsola(solver));

		solver.organizarGrupos();
		System.out.println("***\n" + solver.toString());
	}

}
