package logic;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ArmadoGruposTest
{
	ArmadoGrupos armadoGrupos;
	
	@Before
	public void inicializar() 
	{
		ArrayList<Equipo> equipos = new ArrayList<Equipo>();
		
		equipos.add(new Equipo("Argentina", 8));
		equipos.add(new Equipo("Ecuador", 5));
		equipos.add(new Equipo("Paraguay",5));
		equipos.add(new Equipo("Chile",8));
		equipos.add(new Equipo("Bolivia", 3));
		equipos.add(new Equipo("Brasil",9));
		equipos.add(new Equipo("Venezuela",5));
		equipos.add(new Equipo("Colombia", 9));
		equipos.add(new Equipo("Peru",4));
		equipos.add(new Equipo("Qutar",3));
		equipos.add(new Equipo("Uruguay",6));
		equipos.add(new Equipo("Japon",4));;

		this.armadoGrupos = new ArmadoGrupos(equipos);
	}
	
	@Test
	public void menorDispersionGruposTest() 
	{
		this.armadoGrupos.organizarGrupos();
		assertTrue(this.armadoGrupos.menorDispersionGrupos()==5.666666666666667);
	}
	
	@Test(expected = RuntimeException.class)
	public void menorDispersionSinAsignacionTest() 
	{
		this.armadoGrupos.menorDispersionGrupos();
	}
	
	@Test
	public void mayorDispersionGruposTest() 
	{
		this.armadoGrupos.organizarGrupos();
		assertTrue(this.armadoGrupos.mayorDispersionGrupos()==6.25);
	}
	
	@Test(expected = RuntimeException.class)
	public void mayorDispersionSinAsignacionTest() 
	{
		this.armadoGrupos.mayorDispersionGrupos();
	}
}
