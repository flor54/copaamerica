package logic;

import java.util.ArrayList;

public class Grupo
{
	private ArrayList<Equipo> equipos;
	private double dispersion;
	private double promedio;
	private int CANTIDADEQUIPOS;
	
	public Grupo()
	{
		this.equipos = new ArrayList<Equipo>();
		this.CANTIDADEQUIPOS = 4;
	}
	
	public void agregarEquipo(Equipo e) 
	{
		verificarGrupoIncompleto();
		this.equipos.add(e);
	}

	public void agregarEquipo(String nombre, int fuerza)
	{
		verificarGrupoIncompleto();
		this.equipos.add(new Equipo(nombre,fuerza));
	}
	
	public ArrayList<Equipo> getEquipos()
	{
		return this.equipos;
	}

	public double getDispersion()
	{
		return this.dispersion;
	}
	
	public int getCantidadEquipos()
	{
		return this.CANTIDADEQUIPOS;
	}

	public void calcularValores()
	{
		verificarGrupoCompleto();
		calcularPromedio();
		calcularDispersion();
	}
	
	private void calcularPromedio()
	{
		double sumaFuerzaTotal = 0;
		for(Equipo e : this.equipos)
		{
			sumaFuerzaTotal+=e.getCoeficienteFuerza();
		}
		this.promedio = sumaFuerzaTotal / getCantidadEquipos();
	}
	
	private void calcularDispersion() 
	{
		verificarGrupoCompleto();
		double sumaDispersion = 0;
		for(int i = 0; i < getCantidadEquipos(); i++)
			sumaDispersion+=Math.pow((getEquipoFuerza(i) - this.promedio), 2);
		this.dispersion = sumaDispersion / (getCantidadEquipos()-1);
	}
	
	public Equipo getEquipo(int i)
	{
		verificarIndiceEquipo(i);
		return this.equipos.get(i);
	}

	public boolean estaEquipo(Equipo e)
	{
		for(Equipo equipo : this.equipos) 
		{
			if(e.getEquipo().equals(equipo.getEquipo()) && e.getCoeficienteFuerza() == equipo.getCoeficienteFuerza())
				return true;
		}
		return false;
	}
	
	private double getEquipoFuerza(int i)
	{
		verificarIndiceEquipo(i);
		return getEquipo(i).getCoeficienteFuerza();
	}
	
	@Override
	public String toString() 
	{
		String ret = "";
		for(Equipo e: this.equipos)
		{
			ret = ret + e.toString() + "\n";
		}
		if(dispersion != 0.0) 
			ret = ret + "Dispersion: " + dispersion + "\n\n";
		return ret;
	}

	private void verificarGrupoIncompleto()
	{
		if(this.equipos.size() == getCantidadEquipos())
			throw new RuntimeException("El grupo ya esta completo");
	}
	
	private void verificarGrupoCompleto()
	{
		if(this.equipos.size() != getCantidadEquipos())
			throw new RuntimeException("El grupo esta incompleto");
	}

	private void verificarIndiceEquipo(int indice)
	{
		if(indice >= getCantidadEquipos())
			throw new IllegalArgumentException("Indice excede rango");
	}
}
