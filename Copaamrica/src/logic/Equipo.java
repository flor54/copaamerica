package logic;

public class Equipo
{
	private String equipo;
	private int coeficienteFuerza;
	
	public Equipo(String equipo, int coeficiente)
	{
		nombreInvalido(equipo);
		coeficienteInvalido(coeficiente);
		this.equipo = equipo;
		this.coeficienteFuerza = coeficiente;
	}

	public String getEquipo()
	{
		return equipo;
	}

	public void setEquipo(String equipo)
	{
		nombreInvalido(equipo);
		this.equipo = equipo;
	}

	public int getCoeficienteFuerza()
	{
		return coeficienteFuerza;
	}

	public void setCoeficienteFuerza(int coeficienteFuerza)
	{
		coeficienteInvalido(coeficienteFuerza);
		this.coeficienteFuerza = coeficienteFuerza;
	}
	
	@Override
	public String toString() 
	{
		return "Equipo: " +  this.equipo + ", Coeficiente de Fuerza: "  + this.coeficienteFuerza;
	}
	
	private void nombreInvalido(String nombre) 
	{
		if(nombre.equals("") || nombre.equals(null))
			throw new IllegalArgumentException("Nombre equipo invalido: "+ nombre);
	}
	
	
	private void coeficienteInvalido(int c) 
	{
		if( c <= 0 || c > 10)
			throw new IllegalArgumentException("Coeficiente equipo invalido: "+ c);
	}
}
