package logic;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class GrupoTest
{
	Grupo grupo;
	Equipo[] equipos;
	
	@Before
	public void inicializar() 
	{
		Equipo e1 = new Equipo("Argentina", 8);
		Equipo e2 = new Equipo("Ecuador", 5);
		Equipo e3 =new Equipo("Paraguay",5);
		Equipo e4 =new Equipo("Chile",8);
		
		this.equipos = new Equipo[4];
		this.equipos[0] = e1;
		this.equipos[1] = e2;
		this.equipos[2] = e3;
		this.equipos[3] = e4;
		
		Grupo ret = new Grupo();
		for (int i = 0; i < equipos.length; i++)
			ret.agregarEquipo(equipos[i]);
		
		this.grupo = ret;
	}

	@Test
	public void CalcularDispersionTest() 
	{
		grupo.calcularValores();
		assertEquals(3 ,grupo.getDispersion(),0.0001);
	}

	@Test
	public void agregarEquipoPorNombreYFuerzaTest()
	{
		Grupo grupo = new Grupo();
		grupo.agregarEquipo("Argentina", 10);
		assertTrue(grupo.estaEquipo(new Equipo("Argentina", 10)));
	}
	
	@Test(expected = RuntimeException.class)
	public void agregarEquipoCompletoTest()
	{
		grupo.agregarEquipo(new Equipo("Peru",5));
	}
	
	@Test
	public void getEquiposTest()
	{
		ArrayList<Equipo> equiposBuscados = new ArrayList<Equipo>();
		for(int i = 0; i < this.equipos.length; i++)
			equiposBuscados.add(equipos[i]);
		ArrayList<Equipo> equiposExistentes = this.grupo.getEquipos();
		for(int i = 0; i < equiposBuscados.size(); i++)
			assertTrue(equiposExistentes.contains(equiposBuscados.get(i)));
	}
	
	@Test
	public void estaEquiposFalseTest()
	{
		Equipo equipo = new Equipo("Zimbawe", 5);
		assertFalse(this.grupo.estaEquipo(equipo));
	}
	
	@Test (expected = RuntimeException.class)
	public void verificarGrupoCompletoTest()
	{
		Equipo equipo = new Equipo("Zimbawe", 5);
		this.grupo.agregarEquipo(equipo);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void verificarIndiceEquipoTest()
	{
		@SuppressWarnings("unused")
		Equipo equipo = this.grupo.getEquipo(5);
	}
}
