package logic;

import org.junit.Test;

public class EquipoTest
{
	@Test (expected = IllegalArgumentException.class)
	public void NombreValidoTest()
	{
		@SuppressWarnings("unused")
		Equipo equipo = new Equipo("",3);
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void LimiteCoeficiente1Test()
	{
		@SuppressWarnings("unused")
		Equipo equipo = new Equipo("Argentina",0);
	}

	@Test (expected = IllegalArgumentException.class)
	public void LimiteCoeficiente2Test()
	{
		@SuppressWarnings("unused")
		Equipo equipo = new Equipo("Argentina",11);
	}
}
