package logic;

import java.util.ArrayList;

public class ArmadoGrupos
{
	// clase solver que se encarga de resolver el problema algoritmico que se requiere de acuerdo a la especificacion que se proveyo.
	private ArrayList<Equipo> equipos;
	private ArrayList<Grupo> grupos;
	private ArrayList<Grupo> gruposAuxiliares;
	private ArrayList<Grupo> gruposPosibles;
	
	public ArrayList<Grupo> getGrupos()
	{
		return grupos;
	}

	private int CANTIDADGRUPOS;
	
	// Estadisticas de la simulacion
	private int iteracion;
	private long inicio;
	private double tiempo;
	// Coleccion de observadores
	private ArrayList<Observer> observadores;
		
	// Interfaz para observadores
	public static interface Observer
	{
		public void update();
	}

	public void registrar(Observer observador)
	{
		observadores.add(observador);
	}
	
	private void notificarObservadores()
	{
		for(Observer observador: observadores)
			observador.update();
	}
	
	public ArmadoGrupos(ArrayList<Equipo> equipos)
	{
		validarEquipos(equipos);
		validarCantidadEquipos(equipos);

		this.CANTIDADGRUPOS = 3;
		
		this.equipos = new ArrayList<Equipo>();
		
		for(Equipo e : equipos)
			this.equipos.add(e);
		
		observadores = new ArrayList<Observer>();
	}
	
	public int getIteracion() {
		return iteracion;
	}

	public long getInicio() {
		return inicio;
	}

	public double getTiempo() {
		return tiempo;
	}
	
	public double mayorDispersionGrupos() 
	{
		gruposNoAsignados();
		double mayorDispersion = Double.NEGATIVE_INFINITY;
		double dispersion;
		for(int i = 0; i < this.CANTIDADGRUPOS; i++)
		{
			dispersion = this.grupos.get(i).getDispersion(); 
			if(dispersion > mayorDispersion)
				mayorDispersion = dispersion;
		}
		return mayorDispersion;
	}
	
	public double menorDispersionGrupos() 
	{	
		gruposNoAsignados();
		//esta funcion asume que los grupos ya calcularon sus valores
		double menorDispersion = Double.POSITIVE_INFINITY;
		double dispersion;
		for(int i = 0; i < this.CANTIDADGRUPOS; i++)
		{
			dispersion = this.grupos.get(i).getDispersion(); 
			if(dispersion < menorDispersion)
				menorDispersion = dispersion;
		}
		return menorDispersion;
	}
	
	private double mayorDispersionGruposAuxiliares()
	{
		//esta funcion asume que los grupos auxiliares ya calcularon sus valores
		double mayorDispersion = Double.NEGATIVE_INFINITY;
		double dispersion;
		for(int i = 0; i < this.CANTIDADGRUPOS; i++)
		{
			dispersion = this.gruposAuxiliares.get(i).getDispersion(); 
			if(dispersion > mayorDispersion)
				mayorDispersion = dispersion;
		}
		return mayorDispersion;
	}
	
	private double menorDispersionGruposAuxiliares()
	{
		//esta funcion asume que los grupos auxiliares ya calcularon sus valores
		double menorDispersion = Double.POSITIVE_INFINITY;
		double dispersion;
		for(int i = 0; i < this.CANTIDADGRUPOS; i++)
		{
			dispersion = this.gruposAuxiliares.get(i).getDispersion(); 
			if(dispersion < menorDispersion)
				menorDispersion = dispersion;
		}
		return menorDispersion;
	}
	
	public void organizarGrupos()
	{
		inicializarEstadisticas();
		calcularConfiguracionesPosibles();
		evaluarGruposPosibles();
	}
	
	//Calcula todas las combinaciones posibles de grupos
	// los limites de cada for sirven para que no haya grupos repetidos en n set de 4 
	private void calcularConfiguracionesPosibles()
	{
		ArrayList<Grupo> configuraciones = new ArrayList<Grupo>();
		
		int cantidadEquipos = this.equipos.size();
		for(int i = 0; i < cantidadEquipos-this.CANTIDADGRUPOS; i++)
		{
			for(int j = i+1; j < cantidadEquipos-(this.CANTIDADGRUPOS-1); j++)
			{
				for(int k = j+1; k < cantidadEquipos-1; k++)
				{
					for(int l = k+1; l < cantidadEquipos; l++)
					{
						Grupo grupo = crearGrupo(i, j, k, l);
						grupo.calcularValores();
						configuraciones.add(grupo);
					}
				}
			}
		}
		this.gruposPosibles = configuraciones;
	}

	private Grupo crearGrupo(int i, int j, int k, int l)
	{
		Grupo grupo = new Grupo();
		grupo.agregarEquipo(this.equipos.get(i));
		grupo.agregarEquipo(this.equipos.get(j));
		grupo.agregarEquipo(this.equipos.get(k));
		grupo.agregarEquipo(this.equipos.get(l));
		return grupo;
	}
	
	//Evalua cual trio de todos los grupos posibles es el mejor
	private void evaluarGruposPosibles()
	{
		int cantidadGruposPosibles = this.gruposPosibles.size();
		
		for(int i = 0; i < cantidadGruposPosibles-(this.CANTIDADGRUPOS-1); i++)
		{
			for(int j = i+1; j < cantidadGruposPosibles-1; j++)
			{
				for(int k = j+1; k < cantidadGruposPosibles; k++)
					evaluarGruposAuxiliares(i, j, k);
			}
		}
	}

	private void evaluarGruposAuxiliares(int i, int j, int k) {
		this.gruposAuxiliares = crearConjuntoGrupos(i,j,k);
		if(chequearConflictoGrupos())
			this.gruposAuxiliares = null;
		else
		{
			if(this.grupos == null)
			{
				this.grupos = this.gruposAuxiliares;
				actualizarEstadisticas();
			}
			else
			{
				if(menorDispersionGrupos() < menorDispersionGruposAuxiliares())
				{
					this.grupos = this.gruposAuxiliares;
				}
				else if(menorDispersionGrupos() == menorDispersionGruposAuxiliares())
				{
					this.grupos = (mayorDispersionGrupos() < mayorDispersionGruposAuxiliares() ? this.gruposAuxiliares : this.grupos);
				}
				actualizarEstadisticas();
			}
		}
	}
	
	private ArrayList<Grupo> crearConjuntoGrupos(int i, int j, int k)
	{
		ArrayList<Grupo> grupos = new ArrayList<Grupo>();
		grupos.add(this.gruposPosibles.get(i));
		grupos.add(this.gruposPosibles.get(j));
		grupos.add(this.gruposPosibles.get(k));
		return grupos;
	}

	//Chequea que no se superpongan equipos en los grupos auxiliares
	private boolean chequearConflictoGrupos()
	{
		ArrayList<Equipo> equiposIncluidos = new ArrayList<Equipo>();
		for(int i = 0; i < this.CANTIDADGRUPOS; i++)
			equiposIncluidos.addAll(this.gruposAuxiliares.get(i).getEquipos());
		int equipo = 0;
		boolean incluido = true;
		while(equipo < this.equipos.size() && incluido)
		{
			incluido = incluido && equiposIncluidos.contains(this.equipos.get(equipo));
			equipo++;
		}
		return !incluido;
	}

	private void validarEquipos(ArrayList<Equipo> e) 
	{
		if(e.equals(null) || e.size() == 0) 
			throw new IllegalArgumentException("Se necesitan equipos para armar grupos");
	}
	
	private void validarCantidadEquipos(ArrayList<Equipo> e)
	{
		if(e.size() < 12)
			throw new IllegalArgumentException("Se necesitan 12 equipos en total");
	}
	
	@Override
	public String toString()
	{
		gruposNoAsignados();
		
		String ret = "Resultado FINAL:\n";
		for (int i = 0; i < this.grupos.size(); i++) 
		{
			Grupo g = this.grupos.get(i);
			if(i == 0) 
				ret = ret + "GRUPO A:\n" + g.toString();
			else if(i==1)
				ret = ret + "GRUPO B:\n" + g.toString();
			else
				ret = ret + "GRUPO C:\n" + g.toString();
		}
		ret = ret + "Mayor dispersion: " + this.mayorDispersionGrupos();
		ret = ret + "\nMenor dispersion: " + this.menorDispersionGrupos();
		
		return ret;
	}
	
	private void gruposNoAsignados() 
	{
		if(grupos.equals(null) || this.grupos.size() != this.CANTIDADGRUPOS)
			throw new RuntimeException("Grupos no asignados. Debe asignarlos para operar.");
	}
	
	private void inicializarEstadisticas()
	{
		iteracion = 0;
		inicio = System.currentTimeMillis();
	}
	
	public void actualizarEstadisticas() 
	{
		this.iteracion++;
		this.tiempo = (System.currentTimeMillis() - inicio) / 1000.0;
		notificarObservadores();
	}
}
